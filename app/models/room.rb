class Room < ActiveRecord::Base
	extend FriendlyId

	belongs_to :user
	has_many :reviews, dependent: :destroy

	has_many :reviewed_rooms, through: :reviews, source: :room
	
	validates_presence_of :title, :location, :description
	validates_presence_of :slug
	validates_length_of :description, minimum: 30, allow_blank: false

	mount_uploader :picture, PictureUploader
	friendly_id :title, use: [:slugged, :history]

	
	 def self.most_recent
    	order(created_at: :desc)
	 end


	def complete_name
		"#{title}, #{location}"
	end

	def self.search(query)
		if query.present?
			where(['location LIKE :query OR title LIKE :query OR description LIKE :query',query: "%#{query}%"])
		else
			all
		end
	end

end
