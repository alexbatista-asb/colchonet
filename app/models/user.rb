class User < ActiveRecord::Base
	EMAIL_REGEXP = /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/

	has_many :reviews, dependent: :destroy
	has_many :rooms, dependent: :destroy

	scope :confirmed, -> {where.not(confirmed_at: nil)} # restringe o escopo aos usuários que já confirmaram o email de validação

	validates_presence_of :email, :full_name, :location
	#validates_confirmation_of :password
	validates_length_of :bio, minimum: 30, allow_blank: false
	validates_uniqueness_of :email
	
	has_secure_password
	
	validate :email_format

	before_create do |user|
		user.confirmation_token = SecureRandom.urlsafe_base64
	end

	def self.authenticate(email,password)
		confirmed.find_by(email: email).try(:authenticate,password)
		#user = confirmed.find_by(email: email)
		#if user.present?
		#	user.authenticate(password)
		#end
	end

	def confirm!
		return if confirmed?

		self.confirmed_at = Time.current
		self.confirmation_token = ''
		save!
	end

	def confirmed?
		confirmed_at.present?
	end
	
	private
	#Essa validação pode ser representada da seguinte forma:
	#validates_format_of :email,with: EMAIL_REGEXP
	def email_format
		errors.add(:email, :invalid) unless email.match(EMAIL_REGEXP)
	end
end
